<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificados', function (Blueprint $table) {
            $table->id();
            $table->integer('status_id');
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->string('nome_curso');
            $table->string('instituicao');
            $table->integer('carga_horaria');
            $table->string('justificativa_recusa')->nullable();
            $table->string('cod_verificacao')->unique()->nullable();
            $table->string('caminho_arquivo')->unique()->nullable();
            $table->integer('aluno_id');
            $table->foreign('aluno_id')->references('id')->on('usuarios');
            $table->integer('validador_id');
            $table->foreign('validador_id')->references('id')->on('usuarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificados');
    }
}
