<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Rota;
use App\Perfil;

class RotasInsertDefaultValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $aluno = Perfil::where('valor','ALUNO')->first()->id;
        $aprovador = Perfil::where('valor','APROVADOR')->first()->id;

        $rota = new Rota('aluno', $aluno, 'Dashboard', 'fa-chart-pie');
        $rota->save();

        $rota = new Rota('aluno/certificado', $aluno, 'Certificados', 'fa-graduation-cap');
        $rota->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
