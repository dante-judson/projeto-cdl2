<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Status;

class StatusesInsertDefaultValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $status = new Status('PENDENTE', 'Certificado enviado para aprovador, aguardando aprovação');
        $status->save();
        
        $status = new Status('APROVADO', 'Certificado aprovado');
        $status->save();
        
        $status = new Status('REJEITADO', 'Certificado rejeitado pelo aprovador, favor conferir dados e submeter novamente');
        $status->save();
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
