<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Perfil;

class PerfilsInsertDefaultValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $perfil = new Perfil();
        $perfil->valor = 'ALUNO';
        $perfil->descricao = 'Perfil que será cedido aos alunos: acesso ao painel de alunos';
        $perfil->save();
        
        $perfil = new Perfil();
        $perfil->valor = 'APROVADOR';
        $perfil->descricao = 'Perfil que será cedido aos funcinarios da cdl com permissaão para aprovar ou rejeitar certificados.';
        $perfil->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        
    }
}
