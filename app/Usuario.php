<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Usuario extends Model
{
    public function perfil() {
        return $this->belongsTo('App\Perfil');
    }

    public function rotas() {
        return $this->hasMany('App\Rota');
    }

    public static function encryptPassword ($rawPass) {
        $options = [
            'salt' => Usuario::generateSalt()
        ];

        return password_hash($rawPass, PASSWORD_BCRYPT);
    }

    public static function login ($login, $senha ) {
        try {
            $usuario = Usuario::where('login', $login)->firstOrFail();
            return Usuario::checkPassword($senha, $usuario->senha);
        } catch (ModelNotFoundException $e) {
            error_log($e->getMessage());
            return false;
        }
    }

    public static function getUsuarioByLogin($login) {
        try {
            $usuario = Usuario::where('login', $login)->firstOrFail();
            return $usuario;
        } catch(ModelNotFoundException $e) {
            throw new Exception($e->getMessage());
        }
    }

    public static function checkPassword ($rawPass, $passwordHash ) {
        return password_verify($rawPass, $passwordHash);
    }

    private static function generateSalt () {
        return rand(22,100);
    }
}
