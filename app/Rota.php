<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rota extends Model
{
    function __construct() {

        if (func_num_args() == 4) {
            $this->path = func_get_arg(0);
            $this->perfil_id = func_get_arg(1);
            $this->label = func_get_arg(2);
            $this->icone = func_get_arg(3);
        }
    }
}
