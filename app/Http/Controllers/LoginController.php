<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuario;

class LoginController extends Controller
{
    public function login(Request $request) {
        $login = $request->input('login');
        $senha = $request->input('senha');

        $autorizado = Usuario::login($login, $senha);
        if ($autorizado) {
            try {
                $usuario = Usuario::getUsuarioByLogin($login);
                session(['usuarioLogado'=>$usuario]);
                dd($usuario->rotas);
                $perfil = $usuario->perfil->valor;
                if ($perfil == 'APROVADOR') {
                    return redirect('/aprovador');
                } else if ($perfil == 'ALUNO') {
                    return redirect('/aluno');
                }
            } catch (Exception $e) {
                error_log($e->getMessage());
                return back()->with('erro','Houve um erro ao efetuar o seu login.');
            }
        } else {
            return back()->with('erro','Usuario ou senha inválidos');
        }

    }
    
}
