<?php

namespace App\Http\Middleware;

use Closure;
use App\Usuario;
use App\Rota;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $path = $request->path();
        if ($path == '/' or $path == 'login') {
            return $next($request);
        }
        $perfil = session('usuarioLogado')->perfil_id;
        $rotasPermitidas = Rota::where('perfil_id',$perfil)->get();
        foreach ($rotasPermitidas as $rota) {
            if ($rota->path == $path) {
                return $next($request);
            }
        }

        return redirect('/');
    }
}
