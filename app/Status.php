<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{

    function __construct($valor, $descricao) {
        $this->valor = $valor;
        $this->descricao = $descricao;
    }
}
