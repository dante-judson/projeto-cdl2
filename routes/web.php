<?php

use Illuminate\Support\Facades\Route;

use App\Usuario;
use App\Perfil;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/aluno', [ 'uses' => 'AlunoController@dashboard' ]);

Route::get('/adm/usuario', [ 'uses' => 'UsuarioController@listaUsuarios' ]);

Route::get('/adm/usuario/form/{id?}', [ 'uses' => 'UsuarioController@formUsuario']);

Route::post('/adm/usuario/upsert', [ 'uses' => 'UsuarioController@upsert' ]);

Route::post('/login', [ 'uses' => 'LoginController@login' ]);