@extends('template')

@section('titulo','Listar usuarios')

@section('page-header')
    <h1 class="h3 mb-2 text-gray-800">Lista de Usuários</h1>
    <a href="/adm/usuario/form" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-user-plus fa-sm text-white-50"></i> Adicionar Usuário </a>
@endsection


@section('conteudo')
    
    @if (!empty(session('mensagem')))
    <div class="alert alert-success" role="alert">
        {{ session('mensagem') }}
    </div>
    @endif

    @if(empty($usuarios) or sizeof($usuarios) == 0)
        Nenhum usuário cadastrado.
    @else
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <th>Login</th>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>CPF</th>
                        <th>Perfil</th>
                        <th>Editar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($usuarios as $usuario)
                       
                        <tr>
                            <td>{{ $usuario->login}} </td>
                            <td>{{ $usuario->nome}} </td>
                            <td>{{ $usuario->email}} </td>
                            <td>{{ $usuario->cpf}} </td>
                            <td>{{ $usuario->perfil->valor}} </td>
                            <td>
                                <a href="/adm/usuario/form/{{ $usuario->id }}" 
                                class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                                    <i class="fas fa-edit fa-sm"></i> 
                                </a>
                            </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection