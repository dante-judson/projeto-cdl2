@extends('template')

@section('titulo','Dashboard')

@section('page-header')
    <h1 class="h3 mb-2 text-gray-800">Dashboard</h1>
@endsection

@section('conteudo')
    <div class="row justify-content-center">
    
        @component('components.smallCard')
            @slot('titulo','Certificados Pendentes')
            @slot('valor','0');
            @slot('icon', 'fa-graduation-cap')
            @slot('cor','warning')
        @endcomponent

        @component('components.smallCard')
            @slot('titulo','Certificados Aprovados')
            @slot('valor','0');
            @slot('icon', 'fa-check')
            @slot('cor','success')
        @endcomponent

        @component('components.smallCard')
            @slot('titulo','Horas Contabilizadas')
            @slot('valor','0');
            @slot('icon', 'fa-clock')
            @slot('cor','info')
        @endcomponent

    </div>


    <div class="row justify-content-center">
    
    <div class="col-lg-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">  
                <h6 class="m-0 font-weight-bold text-primary">Carga horário concluída</h6>
            </div>
            <div class="card-body">
                <div class="chart-pie pt-4">
                    <canvas id="myPieChart"></canvas>
                </div>
            </div>
        </div>
    </div>


    </div>

@endsection

<script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script>
